<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_before.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/sale/include.php';

/** @var CMain $APPLICATION */
$saleModulePermissions = $APPLICATION->GetGroupRight('sale');

/** @var CUser $USER */
if (!$USER->IsAdmin()) {
    $APPLICATION->AuthForm(GetMessage('ACCESS_DENIED'));
}

IncludeModuleLangFile(__FILE__);

$APPLICATION->SetTitle('Настройки WATTER');

require $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_after.php';
?>
<?php
$moduleName = 'watter';
$settings = array(
    array(
        'LABEL' => 'Логин CMC turbosms.ua',
        'FORM_NAME' => 'watter-login-sms',
        'OPTION_NAME' => 'LOGIN_SMS',
    ),
    array(
        'LABEL' => 'Пароль turbosms.ua',
        'FORM_NAME' => 'watter-password-sms',
//        'TYPE' => 'password',
        'OPTION_NAME' => 'PASSWORD_SMS',
    ),
    array(
        'LABEL' => 'APIKEY "Нова Пошта"',
        'FORM_NAME' => 'watter-api-np',
//        'TYPE' => 'password',
        'OPTION_NAME' => 'APIKEY_NP',
    ),
    array(
        'LABEL' => 'EMAIL для уведомлений',
        'FORM_NAME' => 'watter-api-email',
//        'TYPE' => 'password',
        'OPTION_NAME' => 'EMAIL',
    ),
);
if (isset($_REQUEST['save']) && strlen($_REQUEST['save']) > 0) {
    foreach ($settings as $setting) {
        if (isset($_REQUEST[$setting['FORM_NAME']])) {
            COption::SetOptionString($moduleName, $setting['OPTION_NAME'], $_REQUEST[$setting['FORM_NAME']]);
        }
    }
}
?>
    <style>
        .form-group {
            display: block;
            margin: 3px;
        }
        .form-group > label {
            display: inline-block;
            min-width: 200px;
        }
    </style>
    <form method="POST" action="<?= $APPLICATION->GetCurUri()?>">
        <?php foreach ($settings as $setting) :?>
            <div class="form-group" title="<?=$setting['DESCRIPTION']?>">
                <label for="<?=$setting['FORM_NAME']?>"><?=$setting['LABEL']?>:</label>
                <input type="<?=$setting['TYPE']?:'text'?>"
                       name="<?=$setting['FORM_NAME']?>" id="<?=$setting['FORM_NAME']?>"
                       value="<?=COption::getOptionString($moduleName, $setting['OPTION_NAME']);?>"
                       placeholder="<?=$setting['DESCRIPTION']?>"
                >
            </div>
        <?php endforeach;?>
        <div class="form-group">
            <input type="submit" class="adm-btn" name="save" title="Сохранить" value="Сохранить">
        </div>
    </form>
<?php
require $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_admin.php';