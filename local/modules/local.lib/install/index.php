<?

Class local_lib extends CModule
{
    var $MODULE_ID = "local.lib";
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $MODULE_CSS;

    function local_lib()
    {
        $arModuleVersion = array();

        $path = str_replace("\\", "/", __FILE__);
        $path = substr($path, 0, strlen($path) - strlen("/index.php"));
        include($path."/version.php");

        if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion))
        {
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        }

        $this->MODULE_NAME = "local.lib";
        $this->MODULE_DESCRIPTION = "WatterApi";
    }

    function InstallFiles($arParams = array())
    {
        return true;
    }

    function UnInstallFiles()
    {
        return true;
    }

    function DoInstall()
    {
        global $DOCUMENT_ROOT, $APPLICATION;
        RegisterModule("local.lib");
        $APPLICATION->IncludeAdminFile("Установка модуля local.lib", $DOCUMENT_ROOT."/local/modules/local.lib/install/step.php");
    }

    function DoUninstall()
    {
        global $DOCUMENT_ROOT, $APPLICATION;
        UnRegisterModule("local.lib");
        $APPLICATION->IncludeAdminFile("Деинсталляция модуля local.lib", $DOCUMENT_ROOT."/local/modules/local.lib/install/unstep.php");
    }
}
?>