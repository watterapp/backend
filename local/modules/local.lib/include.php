<?php
/**
 * Created by PhpStorm.
 * User: dmytromaksiutenko
 * Date: 29.07.16
 * Time: 14:06
 */
?>
<?
use Bitrix\Main\Type\DateTime;

//
function testAgent()
{
    mail('dibarboss@gmail.com', 'АгентАгент', 'Агент');
    return "testAgent();";
}


function checkFilterDay()
{
    CModule::IncludeModule("highloadblock");
    $hlblock = Bitrix\Highloadblock\HighloadBlockTable::getById(9)->fetch();
    $entity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
    $entity_data_class = $entity->getDataClass();

    $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getList(
        array("filter" => array('=TABLE_NAME' => 'api_watter_client_filters'))
    )->fetch();

    $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
    $Query = new \Bitrix\Main\Entity\Query($entity);
    $Query->setSelect(['*']);
    $Query->setFilter(['UF_IN_USE' => 1, '=UF_RESET' => false]);
    $Query->setOrder([]);

    $result = $Query->exec();

    $result = new CDBResult($result);


    while ($row = $result->Fetch()) {
        $arResult[] = $row;
    }

    foreach ($arResult as $res) {

        if ($res['UF_END_DATE']) {
            $data['UF_END_DATE'] = intval($res['UF_END_DATE']) - 1;

            if ($data['UF_END_DATE'] == 0)
                $data['UF_RESET'] = 1;
            else
                $data['UF_RESET'] = 0;
            $entity_data_class::update($res['ID'], $data);
        }
    };
    return 'checkFilterDay();';
}

function addNewAnalizes()
{
    CModule::IncludeModule("iblock");

    $hostname = 'mail.voda.com.ua:30667';
    $username = "sitereader";
    $password = "Gravi2203";
    $dbName = "SiteIntegration";
    header("Content-Type: text/html; charset=utf-8");

    $link_db = mssql_connect($hostname, $username, $password);
    if (!$link_db) {
        die('Ошибка соединения: ' . mssql_get_last_message());
    }

    mssql_query("set names 'cp1251'");
    $msdb = mssql_select_db($dbName, $link_db);

    $objDateTime = new DateTime();
    $objDateTime->add("-1 day");
    $yest = $objDateTime->format('Y-m-d');

    $today = new DateTime();
    $today = $today->format("Y-m-d");

    $sql = 'SELECT *  FROM tbl_WaterAnalysis WHERE (CreatedOn BETWEEN "' . $yest . '" AND "' . $today . '") AND Address IS NOT NULL';

    $res = mssql_query($sql);
    if (!$res) {
        echo 'error';
    } else {

        while ($row = mssql_fetch_array($res)) {
            $results[] = $row;
        }

        foreach ($results as $result) {

            foreach ($result as $k => $val) {
                if (true == strpos($k, 'ID')) continue;
                $result[$k] = iconv("cp1251", "utf-8", $val);
            }

            $el = new CIBlockElement;
            $PROP = array();

            $PROP['DocumentNumber'] = $result['DocumentNumber'];
            $PROP['CustomerName'] = $result['CustomerName'];
            $PROP['CustomerEmail'] = $result['CustomerEmail'];
            $PROP['CustomerPhone'] = $result['CustomerPhone'];
            $PROP['CityName'] = $result['CityName'];
            $PROP['RegionName'] = $result['RegionName'];
            $PROP['Address'] = $result['Address'];
            $PROP['SourceTypeName'] = $result['SourceTypeName'];
            $PROP['Temperature'] = $result['Temperature'];
            $PROP['PH'] = $result['PH'];
            $PROP['Hydrocarbonates'] = $result['Hydrocarbonates'];
            $PROP['Chlorides'] = $result['Chlorides'];
            $PROP['Calcium'] = $result['Calcium'];
            $PROP['Aluminium'] = $result['Aluminium'];
            $PROP['Color'] = $result['Color'];
            $PROP['Sulfates'] = $result['Sulfates'];
            $PROP['NonTransparent'] = $result['NonTransparent'];
            $PROP['DryRest'] = $result['DryRest'];
            $PROP['Oxidability'] = $result['Oxidability'];
            $PROP['Electroconductivity'] = $result['Electroconductivity'];
            $PROP['Rigidity'] = $result['Rigidity'];
            $PROP['Alkali'] = $result['Alkali'];
            $PROP['HydrogenSulphide'] = $result['HydrogenSulphide'];
            $PROP['ActiveChlorine'] = $result['ActiveChlorine'];
            $PROP['Nitrates'] = $result['Nitrates'];
            $PROP['Nitrites'] = $result['Nitrites'];
            $PROP['Phosphates'] = $result['Phosphates'];
            $PROP['Silicates'] = $result['Silicates'];
            $PROP['Magnium'] = $result['Magnium'];
            $PROP['Kalium'] = $result['Kalium'];
            $PROP['Ammonium'] = $result['Ammonium'];
            $PROP['TotalIron'] = $result['TotalIron'];
            $PROP['Iron'] = $result['Iron'];
            $PROP['Manganese'] = $result['Manganese'];
            $PROP['Copper'] = $result['Copper'];
            $PROP['Smell'] = $result['Smell'];
            $PROP['Natrium'] = $result['Natrium'];
            $PROP['FixedChlorine'] = $result['FixedChlorine'];
            $PROP['Taste'] = $result['Taste'];
            $PROP['Arsenium'] = $result['Arsenium'];
            $PROP['Polyphosphates'] = $result['Polyphosphates'];
            $PROP['Ortophosphates'] = $result['Ortophosphates'];
            $PROP['Phtorides'] = $result['Phtorides'];
            $PROP['Iron3'] = $result['Iron3'];

            $PROP['CreatedOn'] = ConvertTimeStamp(strtotime($result['CreatedOn']), 'FULL');
            $PROP['ModifiedOn'] = ConvertTimeStamp(strtotime($PROP['ModifiedOn']), 'FULL');

            $lat = 0.0000000;
            $lng = 0.0000000;
            $adress = 'Украина, ' . $PROP['RegionName'] . ', ' . $PROP['CityName'] . ', ' . $PROP['Address'];
            $xml = simplexml_load_file('http://maps.google.com/maps/api/geocode/xml?address=' . urlencode($adress) . '&sensor=false');
            $status = $xml->status;

            if ($status == 'OK') {
                $lat = $xml->result->geometry->location->lat;
                $lng = $xml->result->geometry->location->lng;
                $PROP['LAT'] = $lat;
                $PROP['LNG'] = $lng;
            }

            $arLoadProductArray = Array(
                'CODE' => $result['DocumentNumber'],
                'IBLOCK_SECTION_ID' => false,
                'IBLOCK_ID' => 31,
                'PROPERTY_VALUES' => $PROP,
                'NAME' => 'Анализ ' . $result['DocumentNumber'],
                'ACTIVE' => 'Y',
            );

            $el->Add($arLoadProductArray);
        };
    }

    mssql_close($link_db);
    return 'addNewAnalizes();';
}