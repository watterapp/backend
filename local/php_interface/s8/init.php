<?php
/**
 * Created by PhpStorm.
 * User: dibarboss
 * Date: 27.12.16
 * Time: 01:07
 */
define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"] . "/api/log/" . date("Ymd") . ".log");
define("VERIFY_TABLE_ID", 3);
define("USER_FILTERS_TABLE_ID", 9);
define("FILTER_CHANGE_TABLE_ID", 8);
define("INVITE_FRIEND_TABLE_ID", 4);
define("CALLBACK_TABLE_ID", 13);
define("ORDER_WATER_ANALISIS_TABLE_ID", 11);
define("FILTER_IBLOCK_ID", 15);
define("MESSAGES_TABLE_ID", 14);

//use Bitrix\Highloadblock as HL;
//use Bitrix\Main\Entity;

CModule::IncludeModule("highloadblock");
CModule::IncludeModule("iblock");
//
//function groundhogDay()
//{
//    $hlblock = HL\HighloadBlockTable::getById(9)->fetch();
//    $entity = HL\HighloadBlockTable::compileEntity($hlblock);
//    $entity_data_class = $entity->getDataClass();
//    $res = getHighLoadBlockList('api_watter_client_filters', ['UF_IN_USE' => 1]);
//    foreach($res as $re){
//        if($re['UF_END_DATE']){
//            $data['UF_END_DATE'] = $re['UF_END_DATE']-1;
//            $result = $entity_data_class::update($re['ID'], $data);
//        }else{
//            $data['UF_IN_USE'] = 0;
//            $result = $entity_data_class::update($re['ID'], $data);
//        }
//    };
//}

function getHighLoadBlockList($hlbl, $filter = array('*'), $arSelect = array('*'), $arOrder = array())
{
//    CModule::IncludeModule("highloadblock");
    $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getList(
    array("filter" => array('=TABLE_NAME' => $hlbl))
)->fetch();
    $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
    $Query = new \Bitrix\Main\Entity\Query($entity);
//    pre($filter);
    $Query->setSelect($arSelect);
    $Query->setFilter($filter);
    $Query->setOrder($arOrder);

    $result = $Query->exec();

    $result = new CDBResult($result);
    while ($row = $result->Fetch()) {
        $arResult[] = $row;
    }
    return $arResult;
}

function getFilterResource($id)
{
    $res = \CIBlockElement::GetList([], ['IBLOCK_ID' => FILTER_IBLOCK_ID, '=ID' => $id], false, false, ['PROPERTY_RESOURCE'])->fetch();
    return $res['PROPERTY_RESOURCE_VALUE'];
}

/**
 * @param $arr to sexy style ;)
 */
function pre($arr)
{
    echo '<pre>';
    print_r($arr);
    echo '</pre>';
}
