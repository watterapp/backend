<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">

    <title>Watter | Find your better watter</title>
    <meta name="description" content="Mobile app that controls the quality of your drinking water">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <meta name="apple-itunes-app" content="app-id=1204854954">
    <meta name="google-play-app" content="app-id=ua.ecosoft_aos">
    <link rel="stylesheet" href="libs/jquery-smartbanner/jquery.smartbanner.css" type="text/css" media="screen">


    <meta property=" og:image
    " content="path/to/image.jpg">
    <link rel="shortcut icon" href="img/favicon/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="img/favicon/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/favicon/apple-touch-icon-114x114.png">

</head>

<body>

<header class="header">
    <!--<img class="water-drop" src="img/waterdrop.svg">-->
    <div class="water-drop"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="header-logo-wrapper wrapper">
                    <div class="header-logo logo"></div>
                    <div class="header-iphone-bg"></div>
                </div>
            </div>
            <div class="col-md-6">
                <i class="fa fa-bars header-fa-bars" aria-hidden="true"></i>
                <nav class="header-nav">
                    <ul>
                        <!--li><a href="#">Home</a></li>
                        <li><a href="#">About</a></li>
                        <li><a href="#">Features</a></li>
                        <li><a href="#">Testimonials</a></li-->
                        <li><a href="mailto:company@ecosoft.com">Contact Us</a></li>
                    </ul>
                </nav>
                <h1 class="header-title">Watter.<br>
                    <span>Find your<br> better watter</span>
                </h1>
                <p class="header-text">Mobile app that controls the quality of your drinking water</p>
                <div class="header-buttons buttons">
                    <a href="//itunes.apple.com/ua/app/watter/id1204854954?mt=8&ign-mpt=uo%3D4" class="buttons-ios ios"
                       target="_blank"></a><a href="//play.google.com/store/apps/details?id=ua.ecosoft_aos&hl=en"
                                              class="buttons-android android" target="_blank"></a>
                </div>
            </div>
        </div>
    </div>
</header>

<section class="about">
    <div class="drinking-girl">
        <img class="girl" src="img/drinking-girl.png" alt="">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <h2 class="about-title">Water is essential for our life. We drink safe, clean, pure water to stay
                    healthy</h2>
                <p class="about-text">Not many of us realize, that this choice can be harmful for the environment. By
                    purchasing water in the plastic bottle consumers finance intensive creation of non-recyclable
                    plastic waste and contributes to heavy air pollution by CO2. Many of the plastic bottles contains
                    BPA - dangerous chemical, causing significant cancer threat</p>
                <h3 class="about-subtitle">Why people don't drink tap or filtered water?</h3>
                <p class="about-text">We in Watter believe that drinking water from the tap is much better, greener and
                    sustainable choice. In some areas water from the tap is of perfect quality, sometimes it should be
                    improved by appropriate filtration equipment</p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-md-7">
                <p class="warnings-text warnings-text-quality">Most of us don't know for sure what is the quality of
                    tap water in the neighborhood and <span>don't want to put our health on risk</span> by drinking
                    water from tap</p>
            </div>
            <div class="col-sm-6 col-md-5">
                <p class="warnings-text warnings-text-filter">Even if water filter is installed <span>we are not 100% sure
                    that water is good to drink</span> and filters are properly treating your water</p>
            </div>
        </div>
    </div>
</section>

<section class="usage">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <div class="usage-grad-line grad-line"></div>
                <h2 class="usage-title">What if the water we use is not as ideal as we would like?</h2>
                <p class="usage-text">Water app helps to control the quality of water in the tap and ensure that the
                    water you
                    drink is
                    safe and of great taste - the same or better than from the bottle. We collect water samples from
                    volunteered users around the country providing sample collection bottle and water analysis free of
                    charge</p>
                <div class="steps">
                    <div class="steps-first first">
                        <h3 class="first-steps-title steps-title">Check water quality near you</h3>
                        <p class="first-steps-text steps-text">
                            Tap "locate" to learn water quality around you<br>
                            Browse the map to see the quality in the neighborhood<br>
                            Find the address you are interested in.
                        </p>
                        <p class="first-steps-text steps-text">
                            Green markers - water is good, red - dangerous.<br>
                            You can also specify the sources of water such as municipal supply, private well, other
                            sources and specific contaminant you are interested in
                        </p>
                    </div>
                    <div class="steps-grey-line grey-line"></div>
                    <div class="steps-second second">
                        <h3 class="second-steps-title steps-title">Filter water</h3>
                        <p class="second-steps-text steps-text">
                            Tap the marker and see the water quality at specific address.
                            Check the description of the specific contaminant or check the alerts.
                            Tap filter this water – to talk to the water treatment specialist.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="steps-screens"></div>
            </div>
        </div>
    </div>
</section>

<section class="solution">
    <img class="water-drop" src="img/waterdrop.svg">
    <div class="container">
        <div class="row">
            <div class="col-md-offset-6 col-md-6">
                <h2 class="solution-title">Watter app collaborates with major manufacturers of filtration equipment</h2>
            </div>
            <div class="col-md-12">
                <p class="solution-text">If water quality at your place is not at it’s highest standard and you have
                    purchased
                    filtration
                    equipment, <span>Watter app</span> will help you to get your filtered water quality under control
                    as well</p>
            </div>
        </div>
    </div>
</section>

<section class="features">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="features-profile profile">
                    <div class="profile-img"></div>
                    <h3 class="profile-title">My profile</h3>
                    <p class="profile-text">If water quality at your place is not at it’s highest standard and you have
                        purchased filtration equipment, Watter app will help you to get your filtered water quality
                        under control as well. You need to add a filter to your profile, choose the source of your water
                        supply, choose the manufacturer, type and specific model of your filter or scan the barcode or
                        QR code on the box
                    </p>
                </div>
                <div class="features-register register">
                    <div class="register-img"></div>
                    <h3 class="register-title">Register your filter</h3>
                    <p class="register-text">By naming your filter and registering your address or geopositioning with
                        “locate me” tab you would match your filter performance best to the specific water composition
                        in the are it is installed. Choose your installation partner in order to get installer notified
                        that your filters need to be changed</p>
                </div>
                <h2 class="features-text">Watter is encouraging users to register and test drinking water at their
                    location</h2>
                <div class="features-map map">
                    <div class="map-img"></div>
                    <h3 class="map-title">Water quality map</h3>
                    <p class="map-text">After water sample is delivered to Watter major contaminants are analyzed Data
                        is used to build the water quality map which is displayed in the Watter app</p>
                </div>
            </div>
            <div class="col-md-offset-2 col-md-5 features-right">
                <h2 class="features-text">Register and get your
                    filtered water under control</h2>
                <div class="features-filters filters">
                    <div class="filters-img"></div>
                    <h3 class="filters-title">Add your own filters</h3>
                    <p class="filters-text">By choosing private or commercial use of the filter, number of people and
                        presence
                        of kids in your family you would help Watter to calculate your water consumption precisely</p>
                </div>
                <div class="features-replacement replacement">
                    <div class="replacement-img"></div>
                    <h3 class="replacement-title">Filter replacement</h3>
                    <p class="replacement-text">When you filter is registered, the filtration capacity is calculated and
                        the filter replacement countdown starts. Few days before the capacity of the filter is exhausted
                        you are notified that the replacement is needed. You can choose the way we remind you about
                        filter replacement – push notification, email notification or sms</p>
                </div>
                <div class="features-water water">
                    <div class="water-img"></div>
                    <h3 class="water-title">Test your water</h3>
                    <p class="water-text">Watter is encouraging users to register and test drinking water at their
                        location. Tap + test my water to order the delivery of the sample collection bottle to your
                        place or filter my water in order to talk to water treatment specialist</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="download">
    <img class="water-drop" src="img/waterdrop.svg">
    <div class="container">
        <div class="row">
            <div class="col-md-11">
                <h1 class="download-text">
                    <span>Take your drinking water quality under control with</span> Water App
                </h1>
                <div class="download-buttons buttons">
                    <a href="//itunes.apple.com/ua/app/watter/id1204854954?mt=8&ign-mpt=uo%3D4" class="buttons-ios ios"
                       target="_blank"></a><a href="//play.google.com/store/apps/details?id=ua.ecosoft_aos&hl=en"
                                              class="buttons-android android" target="_blank"></a>
                </div>
            </div>
        </div>
    </div>
</section>

<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="footer-logo logo"></div>
            </div>
            <div class="col-md-6">
                <div class="footer-info info">
                    <h3 class="info-title"><a href="mailto:company@ecosoft.com">Contact Us</a></h3>
                    <p class="info-copy">© 2017 SPC Ecosoft LLC. All right reserved</p>
                </div>
            </div>
        </div>
    </div>
</footer>

<link rel="stylesheet" href="css/main.min.css">

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js"></script>
<script src="libs/jquery-smartbanner/jquery.smartbanner.js"></script>
<script type="text/javascript">
    $().smartbanner();
</script>
<script src="//use.fontawesome.com/e99150cbc9.js"></script>
<script src="js/scripts.min.js"></script>

</body>
</html>