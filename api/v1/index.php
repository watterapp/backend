<?php
/**
 * Created by PhpStorm.
 * User: dmytromaksiutenko
 * Date: 22.06.16
 * Time: 15:18
 */
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

header('Content-Type: application/json');


$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
$url = $request->get("_url");
$apiKey = $request->get("apiKey");

$params = $request->getQueryList()->toArray();

if ($url == '/') {
    echo json_encode(array('uptime' => 99.9));
}

if ($url == '/verifyPhone') {
    $phoneNum = $request->get("phone");

    $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById(VERIFY_TABLE_ID)->fetch();
    $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
    $entity_data_class = $entity->getDataClass();

    $sms_code = rand(1000, 9999);


    $sms_body = 'Your code in Watter app: ' . $sms_code;

    $client = new SoapClient('http://turbosms.in.ua/api/wsdl.html');
    $auth = [
        'login' => COption::getOptionString('watter', 'LOGIN_SMS'),
        'password' => COption::getOptionString('watter', 'PASSWORD_SMS')
    ];

    // Авторизируемся на сервере
    $result = $client->Auth($auth);
    $sms = [
        'sender' => 'WATTER',
        'destination' => $phoneNum,
        'text' => $sms_body
    ];
    $result = $client->SendSMS($sms);
    //echo $result->SendSMSResult->ResultArray[0];

    $time = new \Bitrix\Main\Type\DateTime(null, 0);
    $data = array(
        "UF_TIME" => $time,
        "UF_PHONE" => $phoneNum,
        "UF_CODE" => $sms_code,
        "UF_RESULT" => $result->SendSMSResult->ResultArray[0].' | '.$result->SendSMSResult->ResultArray[1],
    );
    $entity_data_class::add($data);
    /*$ID = $result->getId();
    if (CModule::IncludeModule("imaginweb.sms")) {
        $arParams = array(
            'GATE' => 'turbosms.ua',
            'LOGIN' => COption::getOptionString('watter', 'LOGIN_SMS'),
            'PASSWORD' => COption::getOptionString('watter', 'PASSWORD_SMS'),
            'ORIGINATOR' => 'WATTER'
        );

        CIWebSMS::Send($phoneNum, $sms_body, $arParams);


    }*/

//    if ($ID > 0) {
        $response = array(
            'success' => 1,
            'errCode' => 0,
            'errMsg' => ""
        );
//    }
    echo json_encode($response);
    AddMessage2Log(array('ID' => $ID, 'DATA' => $data, 'RESPONSE' => $response, 'SMS'=>$result->SendSMSResult->ResultArray[0]), 'api.verifyPhone');
}

if ($url == '/confirmPhone') {
    $phoneNum = $request->get("phone");
    $code = (int)$request->get("code");
    $uin = $request->get("uin");

    $expireTime = new \Bitrix\Main\Type\DateTime(null, 0);
    $expireTime->add('-1200 second');

    $codeId = getHighLoadBlockList(
        'api_watter_phone_verify',
        array('UF_CODE' => $code, 'UF_PHONE' => $phoneNum, '>=UF_TIME' => $expireTime)
    );

    $response = array(
        'success' => 1,
        'errCode' => 0,
        'errMsg' => "",
    );

    if ($codeId[0]['ID'] > 0 || ($phoneNum == '380071234567' && $code == 7893)) {
        $cUser = new CUser;
        $dbUsers = $cUser->GetList($sort_by, $sort_ord, array('LOGIN' => $phoneNum));
        if ($arUser = $dbUsers->Fetch()) {
            $response['data'] = array('apiKey' => md5($arUser['LOGIN']));
        } else {

            $arFields = Array(
                "LOGIN" => "$phoneNum",
                "LID" => "s8",
                "ACTIVE" => "Y",
                "GROUP_ID" => array(3, 4, 9),
                "PASSWORD" => "$phoneNum.$code",
                "CONFIRM_PASSWORD" => "$phoneNum.$code",
                "UF_API_KEY" => md5($phoneNum),
                "UF_UIN" => $uin,
            );

            $ID = $cUser->Add($arFields);
            $response['data'] = array('apiKey' => md5($phoneNum));
        }
    } else {
        $response = array(
            'success' => 0,
            'errCode' => 404,
            'errMsg' => "Code is wrong",
        );
    }

    echo json_encode($response);
    AddMessage2Log(array('REQUEST' => $params, 'RESPONSE' => $response), 'api.confirmPhone');
}

if ($url == '/register') {
    $name = $request->get("name");
    $lastName = $request->get("lastname");
    $birthDate = $request->get("birthDate");
    $address = $request->get("address");
    $email = $request->get("email");

    $arUser = getUserByApiKey($apiKey);

    if ($arUser['ID'] > 0) {

        $user = new CUser;
        $fields = Array(
            "ACTIVE" => "Y",
            "GROUP_ID" => array(3, 4, 9), // add to group "WATTER"
        );
        if ($name) {
            $fields['NAME'] = $name;
        }
        if ($lastName) {
            $fields['LAST_NAME'] = $lastName;
        }
        if ($birthDate) {
            $fields['PERSONAL_BIRTHDAY'] = $birthDate;
        }
        if ($address) {
            $fields['PERSONAL_NOTES'] = $address;
        }
        if ($email) {
            $fields['EMAIL'] = $email;
        }

        $user->Update($arUser['ID'], $fields);
        $strError .= $user->LAST_ERROR;
        $response = array(
            'success' => 1,
            'errCode' => 0,
            'errMsg' => "$strError",
        );
    } else {
        $response = array(
            'success' => 0,
            'errCode' => 404,
            'errMsg' => "apiKey not found",
        );
    }
    echo json_encode($response);
    AddMessage2Log(array('USER_ID' => $arUser['ID'], 'USER_FIELDS' => $fields, 'REQUEST' => $params, 'RESPONSE' => $response), 'api.register');
}

if ($url == '/uploadUserPhoto') {
    $arUser = getUserByApiKey($apiKey);
    $file = $request->get('file');

    if ($arUser['ID'] > 0) {
//        $f = fopen($_SERVER["DOCUMENT_ROOT"] . CFile::getPath($arUser['PERSONAL_PHOTO']), "rb"); // имя файла или картинки -- открыли файл на чтение
//        $upload = fread($f, filesize($_SERVER["DOCUMENT_ROOT"] . CFile::getPath($arUser['PERSONAL_PHOTO']))); // считали файл в переменную
//        fclose($f);
//        $file = base64_encode($upload);
        //AddMessage2Log($file);

        $img = base64_decode($file);
        $f = finfo_open();
        $mime_type = finfo_buffer($f, $img, FILEINFO_MIME_TYPE);
        $fileType = ($mime_type == 'image/png') ? 'png' : 'jpg';

        $uploadPhoto = $_SERVER["DOCUMENT_ROOT"] . "/api/uploadImg/$apiKey.$fileType";
        $fpng = fopen($uploadPhoto, "w");
        fwrite($fpng, $img);
        fclose($fpng);

        $arFile = CFile::MakeFileArray($uploadPhoto);
        $arFile['del'] = "Y";
        $arFile['old_file'] = $arUser['PERSONAL_PHOTO'];
        $fields['PERSONAL_PHOTO'] = $arFile;

        $user = new CUser;
        $user->Update($arUser['ID'], $fields);
        $strError .= $user->LAST_ERROR;
        $response = array(
            'success' => 1,
            'errCode' => 0,
            'errMsg' => "$strError",
        );
    } else {
        $response = array(
            'success' => 0,
            'errCode' => 404,
            'errMsg' => "apiKey not found",
        );
    }
    echo json_encode($response);
    AddMessage2Log(array('USER_ID' => $arUser['ID'], 'REQUEST' => $params, 'RESPONSE' => $response), 'api.uploadUserPhoto');
}

if ($url == '/getUserProfile') {
    $arUser = getUserByApiKey($apiKey);
//    echo '<pre>';print_r($arUser);
//    if ($arUser = $dbRes->Fetch())
//    {
//        echo $arUser["EMAIL"]." ".$arUser["DATE_REGISTER"]."<br>";
//    }
    if ($arUser['ID'] > 0) {
        $response = array(
            'success' => 1,
            'errCode' => 0,
            'errMsg' => "",
        );
        $response['data']['userProfile'] = array(
            'name' => $arUser['NAME'],
            'email' => $arUser['EMAIL'],
            'birthDate' => $arUser['PERSONAL_BIRTHDAY'],
            'address' => $arUser['PERSONAL_NOTES'],
            'photo' => ($arUser['PERSONAL_PHOTO']) ? CFile::getPath($arUser['PERSONAL_PHOTO']) : '',
            'bonus' => (int)$arUser['UF_BONUS'],
            'autoChangeFilter' => (int)$arUser['UF_AUTO_CF'],
            'receivePushMessages' => (int)$arUser['UF_RECEIVE_PM'],
            'receivePushFilterNeedsChange' => (int)$arUser['UF_RECEIVE_PFNC'],
            'receivePushWaterAnalysisDelivery' => (int)$arUser['UF_RECEIVE_PWAD'],
            'receivePushWaterQualityUpdate' => (int)$arUser['UF_RECEIVE_PWQU'],
            'receivePushFilterQualityUpdate' => (int)$arUser['UF_RECEIVE_PFQU'],
            'receiveSMS' => (int)$arUser['UF_RECEIVE_SMS'],
            'receiveEmail' => (int)$arUser['UF_RECEIVE_EMAIL'],
        );
    } else {
        $response = array(
            'success' => 0,
            'errCode' => 404,
            'errMsg' => "apiKey not found",
        );
    }
    echo json_encode($response);
    AddMessage2Log(array('REQUEST' => $params, 'RESPONSE' => $response), 'api.getUserProfile');
}

if ($url == '/saveUserSettings') {
    $arUser = getUserByApiKey($apiKey);

    if ($arUser['ID'] > 0) {
        if (strlen($request->get('autoChangeFilter')) > 0) {
            $fields['UF_AUTO_CF'] = $request->get('autoChangeFilter');
        }
        if (strlen($request->get('receivePushMessages')) > 0) {
            $fields['UF_RECEIVE_PM'] = $request->get('receivePushMessages');
        }
        if (strlen($request->get('receivePushFilterNeedsChange')) > 0) {
            $fields['UF_RECEIVE_PFNC'] = $request->get('receivePushFilterNeedsChange');
        }
        if (strlen($request->get('receivePushWaterAnalysisDelivery')) > 0) {
            $fields['UF_RECEIVE_PWAD'] = $request->get('receivePushWaterAnalysisDelivery');
        }
        if (strlen($request->get('receivePushWaterQualityUpdate')) > 0) {
            $fields['UF_RECEIVE_PWQU'] = $request->get('receivePushWaterQualityUpdate');
        }
        if (strlen($request->get('receivePushFilterQualityUpdate')) > 0) {
            $fields['UF_RECEIVE_PFQU'] = $request->get('receivePushFilterQualityUpdate');
        }
        if (strlen($request->get('receiveSMS')) > 0) {
            $fields['UF_RECEIVE_SMS'] = $request->get('receiveSMS');
        }
        if (strlen($request->get('receiveEmail')) > 0) {
            $fields['UF_RECEIVE_EMAIL'] = $request->get('receiveEmail');
        }

        $user = new CUser;
        $user->Update($arUser['ID'], $fields);
        $strError .= $user->LAST_ERROR;

        $response = array(
            'success' => 1,
            'errCode' => 0,
            'errMsg' => "$strError",
        );
    } else {
        $response = array(
            'success' => 0,
            'errCode' => 404,
            'errMsg' => "apiKey not found",
        );
    }
    echo json_encode($response);
    AddMessage2Log(array('USER_ID' => $arUser['ID'], 'USER_FIELDS' => $fields, 'REQUEST' => $params, 'RESPONSE' => $response), 'api.saveUserSettings');
}

if ($url == '/getUserFilters') {
    $arUser = getUserByApiKey($apiKey);

    if ($arUser['ID'] > 0) {
        $response = array(
            'success' => 1,
            'errCode' => 0,
            'errMsg' => "",
        );
        $userFilters = getHighLoadBlockList(
            'api_watter_client_filters',
            array('=UF_USER' => $arUser['ID'])
        );
        foreach ($userFilters as $userFilter) {
            $usFilter['id'] = $userFilter['ID'];
            $usFilter['name'] = $userFilter['UF_NAME'];
            $usFilter['filterType'] = $userFilter['UF_FILTER_TYPE'];
            if ($userFilter['UF_INSTALL_DATA'])
                $usFilter['installDate'] = $userFilter['UF_INSTALL_DATA']->toString();
            else
                $usFilter['installDate'] = null;

            $usFilter['inUse'] = $userFilter['UF_IN_USE'];
            $usFilter['reset'] = $userFilter['UF_RESET'];
            $usFilter['change'] = $userFilter['UF_ORDERED'];
            $usFilter['endDay'] = (int)$userFilter['UF_END_DATE'];
            $usFilter['allDay'] = (int)$userFilter['UF_DATE'];

            $response['data']['userFilters'][] = $usFilter;
        }
    } else {
        $response = array(
            'success' => 0,
            'errCode' => 404,
            'errMsg' => "apiKey not found",
        );
    }
    echo json_encode($response);
    AddMessage2Log(array('USER_ID' => $arUser['ID'], 'REQUEST' => $params, 'RESPONSE' => $response), 'api.getUserFilters');
}

if ($url == '/setFilterUsageState') {
    $arUser = getUserByApiKey($apiKey);
    $filterId = $request->get('id');
    $inUse = (int)$request->get('inUse');

    if ($arUser['ID'] > 0) {

        $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById(USER_FILTERS_TABLE_ID)->fetch();
        $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
        $entity_data_class = $entity->getDataClass();

        $data = array(
            "UF_IN_USE" => $inUse,
        );
        $result = $entity_data_class::update($filterId, $data);

        $response = array(
            'success' => 1,
            'errCode' => 0,
            'errMsg' => "",
        );
    } else {
        $response = array(
            'success' => 0,
            'errCode' => 404,
            'errMsg' => "apiKey not found",
        );
    }
    echo json_encode($response);
    AddMessage2Log(array('USER_ID' => $arUser['ID'], 'REQUEST' => $params, 'RESPONSE' => $response), 'api.setFilterUsageState');
}

if ($url == '/orderFilterChange') {
    $arUser = getUserByApiKey($apiKey);
    $filterId = $request->get('id');
    if ($filterId) {
        if ($arUser['ID'] > 0) {
            $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById(FILTER_CHANGE_TABLE_ID)->fetch();
            $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
            $entity_data_class = $entity->getDataClass();

            $data = array(
                "UF_USER" => $arUser['ID'],
                "UF_FILTER" => $filterId,
            );
            $result = $entity_data_class::add($data);

            $hlblock = Bitrix\Highloadblock\HighloadBlockTable::getById(9)->fetch();
            $entity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
            $entity_data_class = $entity->getDataClass();

            $dataF = ['UF_ORDERED' => 1];
            $entity_data_class::update($filterId, $dataF);

            $hlblock = Bitrix\Highloadblock\HighloadBlockTable::getById(14)->fetch();
            $entity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
            $entity_data_class = $entity->getDataClass();

            $dataF = ['UF_USER' => $arUser['ID'], 'UF_MESSAGE' => "Вы заказали замену фильтра", 'UF_TIME' => new \Bitrix\Main\Type\DateTime()];
            $entity_data_class::add($dataF);

            $response = array(
                'success' => 1,
                'errCode' => 0,
                'errMsg' => "",
            );
        } else {
            $response = array(
                'success' => 0,
                'errCode' => 404,
                'errMsg' => "apiKey not found",
            );
        }
    } else {
        $response = array(
            'success' => 0,
            'errCode' => 404,
            'errMsg' => "filterID not found",
        );
    }
    echo json_encode($response);
    AddMessage2Log(array('USER_ID' => $arUser['ID'], 'REQUEST' => $params, 'RESPONSE' => $response), 'api.orderFilterChange');
}

if ($url == '/inviteFriend') {
    $arUser = getUserByApiKey($apiKey);
    $friendPhone = $request->get('phone');

    if ($arUser['ID'] > 0) {
        $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById(INVITE_FRIEND_TABLE_ID)->fetch();
        $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
        $entity_data_class = $entity->getDataClass();

        $data = array(
            "UF_USER" => $arUser['ID'],
            "UF_PHONE" => $friendPhone,
        );
        $result = $entity_data_class::add($data);

        $response = array(
            'success' => 1,
            'errCode' => 0,
            'errMsg' => "",
        );
    } else {
        $response = array(
            'success' => 0,
            'errCode' => 404,
            'errMsg' => "apiKey not found",
        );
    }
    echo json_encode($response);
    AddMessage2Log(array('USER_ID' => $arUser['ID'], 'REQUEST' => $params, 'RESPONSE' => $response), 'api.inviteFriend');
}

if ($url == '/getBonusTasks') {
    $arUser = getUserByApiKey($apiKey);

    if ($arUser['ID'] > 0) {
        $response = array(
            'success' => 1,
            'errCode' => 0,
            'errMsg' => "",
        );

        $arBonus = getHighLoadBlockList('api_watter_bonus_tasks', array('*'));
        if ($arBonus) {
            foreach ($arBonus as $bonus) {
                $arrData['id'] = $bonus['ID'];
                $arrData['bonus'] = $bonus['UF_BONUS'];
                $arrData['description'] = $bonus['UF_DESCRIPTION'];
                $arrData['expireDate'] = $bonus['UF_EXPIRE_DATE']->toString();

                $response['data']['bonusTasks'][] = $arrData;
            }
        } else {
            $response['data']['bonusTasks'] = null;
        }

    } else {
        $response = array(
            'success' => 0,
            'errCode' => 404,
            'errMsg' => "apiKey not found",
        );
    }
    echo json_encode($response);
    AddMessage2Log(array('USER_ID' => $arUser['ID'], 'REQUEST' => $params, 'RESPONSE' => $response), 'api.getBonusTasks');
}

if ($url == '/getFilterByBarcode') {
    $arUser = getUserByApiKey($apiKey);
    $barcode = $request->get('barcode');

    CModule::IncludeModule("iblock");
    if ($arUser['ID'] > 0) {
        if ($barcode > 0) {
//        pre($arUser);
            $response = array(
                'success' => 1,
                'errCode' => 0,
                'errMsg' => "",
                'data' => ['filter' => []],
            );
            $arFilter = [
                "IBLOCK_ID" => FILTER_IBLOCK_ID,
                "=PROPERTY_BARCODE" => $barcode,
                "ACTIVE_DATE" => "Y",
                "ACTIVE" => "Y"];
            $res = CIBlockElement::GetList(
                [],
                $arFilter,
                false,
                array("nTopCount" => 1),
                ['ID', 'NAME', 'IBLOCK_ID', 'DETAIL_TEXT', 'DETAIL_PICTURE']
            );
            if ($res = $res->GetNextElement()) {
                $fields = $res->GetFields();
                $props = $res->GetProperties();

                $response['data']['filter']['id'] = $fields['ID'];
                $response['data']['filter']['name'] = $fields['NAME'];
                $response['data']['filter']['img'] = CFile::getPath($fields['DETAIL_PICTURE']);
                $response['data']['filter']['description'] = $fields['DETAIL_TEXT'];

                $response['data']['filter']['filterType'] = $props['TYPE']['VALUE'];
                $response['data']['filter']['price'] = $props['PRICE']['VALUE'];
                $response['data']['filter']['old_price'] = $props['OLD_PRICE']['VALUE'];
                $response['data']['filter']['currency'] = $props['CURRENCY']['VALUE'];
                $response['data']['filter']['articul'] = $props['ARTICUL']['VALUE'];
                $response['data']['filter']['marketing'] = $props['MARKETING']['VALUE'];
                $response['data']['filter']['barcode'] = $props['BARCODE']['VALUE'];
            } else {
                $response = array(
                    'success' => 0,
                    'errCode' => 404,
                    'errMsg' => "barcode not found",
                );
            }
        } else {

            $response = array(
                'success' => 0,
                'errCode' => 404,
                'errMsg' => "barcode not found",
            );
        }
    } else {
        $response = array(
            'success' => 0,
            'errCode' => 404,
            'errMsg' => "apiKey not found",
        );
    }
    echo json_encode($response);
    AddMessage2Log(array('USER_ID' => $arUser['ID'], 'REQUEST' => $params, 'RESPONSE' => $response), 'api.getFilterByBarcode');
}

if ($url == '/orderFilter') {
    $sourse = [1, 2, 3];
    $arUser = getUserByApiKey($apiKey);
    $filterId = $request->get('id');
    $analizeId = (int)$request->get('analyzeId');
    $filterUsage = $sourse[$request->get('filterUsage')];
    $name = $request->get('name');
    $inUse = $request->get('in_use');
    $daily = $request->get('dailyWaterConsumption');
    $family = $request->get('peopleCount');
    $child = ($request->get('littleСhildren') == 1) ? 'Y' : 'N';
    $address = $request->get('address');
    $parhner = $request->get('partnerId');

    if ($arUser['ID'] > 0) {
        $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById(USER_FILTERS_TABLE_ID)->fetch();
        $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
        $entity_data_class = $entity->getDataClass();

        $filtrresurs = getFilterResource($filterId);
        if ($analizeId == 0) $analizejectkost = 4;
        $rescomplekt = $filtrresurs * 4 / $analizejectkost;
        AddMessage2Log($filtrresurs);
        AddMessage2Log($rescomplekt);

        if ($family > 0) {
            (int)$day = $rescomplekt / ($family * 3);
        } else {
            (int)$day = $rescomplekt / $daily;
        }
        AddMessage2Log($day);

        $data = array(
            "UF_USER" => $arUser['ID'],
            "UF_INSTALL_DATA" => '',
            "UF_IN_USE" => $inUse,
            "UF_ANALIZE" => $analizeId,
            "UF_SOURCE" => $filterUsage,
            "UF_DAILY" => $daily,
            "UF_FAMILY" => $family,
            "UF_CHILD" => $child,
            "UF_NAME" => $name,
            "UF_ADDRESS" => $address,
            "UF_PARTNER" => $parhner,
            "UF_FILTER" => $filterId,
            "UF_END_DATE" => $day,
            "UF_DATE" => $day,
        );
        $result = $entity_data_class::add($data);

        $response = array(
            'success' => 1,
            'errCode' => 0,
            'errMsg' => "",
        );
    } else {
        $response = array(
            'success' => 0,
            'errCode' => 404,
            'errMsg' => "apiKey not found",
        );
    }
    echo json_encode($response);
    AddMessage2Log(array('USER_ID' => $arUser['ID'], 'REQUEST' => $params, 'RESPONSE' => $response), 'api.orderFilter');
}
//checkFilterDay
if ($url == '/checkFilterDay') {
    $arUser = getUserByApiKey($apiKey);

    CModule::IncludeModule("local.lib");
    checkFilterDay();

    $response = array(
        'success' => 1,
        'errCode' => 0,
        'errMsg' => "",
        'data' => null
    );

    echo json_encode($response);
    AddMessage2Log(array('USER_ID' => $arUser['ID'], 'REQUEST' => $params, 'RESPONSE' => $response), 'api.checkFilterDay');
}

if ($url == '/getMapWaterAnalyzes') {

    $response = array(
        'success' => 1,
        'errCode' => 0,
        'errMsg' => "",
    );
    $response['data']['waterAnalyzes'] = [];
    // logic
    $obCache = new CPHPCache();
    $cacheLifetime = 86400;
    $cacheID = 'analyzes';
    $cachePath = '/watter/' . $cacheID;
    if ($obCache->InitCache($cacheLifetime, $cacheID, $cachePath)) {
        $vars = $obCache->GetVars();
        extract($vars);
        $response['data']['waterAnalyzes'] = $vars['arAllAnalizes'];
    } elseif ($obCache->StartDataCache()) {
        CModule::IncludeModule("iblock");
        $res = CIBlockElement::GetList(
            [],
            ["IBLOCK_ID" => 31, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", '!PROPERTY_LAT' => false, "!PROPERTY_SourceTypeName" => false],
            false,
            false,
            []
        );
        $response = array(
            'success' => 1,
            'errCode' => 0,
            'errMsg' => "",
        );
        $sourceType = [
            'Водопровод' => 0,
            'Колодец' => 1,
            'Скважина' => 2,
            'Другой источник' => 3,
        ];
        $arDooble = [];
        while ($ob = $res->GetNextElement()) {
            $arFields = $ob->GetFields();
            $arProp = $ob->GetProperties();

            $arAnalize['id'] = $arFields['ID'];

            $arAnalize['lat'] = $arProp['LAT']['VALUE'];
            $arAnalize['lon'] = $arProp['LNG']['VALUE'];

            if (!$arDooble[$arAnalize['lat']][$arAnalize['lon']]) {
                $arAnalize['address'] = $arProp['CityName']['VALUE'] . ', ' . $arProp['Address']['VALUE'];

                $arAnalize['SourceTypeName'] = $arProp['SourceTypeName']['VALUE'];
                $arAnalize['SourceTypeID'] = $sourceType[$arProp['SourceTypeName']['VALUE']];


                $good = 0;
                $normal = 0;
                $warning = 0;

                /*
                 * Цветность - Color - 20
                 * Мутность - NonTransparent - 0,58
                 * Сухой остаток - DryRest - 1000
                 * Жесткость - Rigidity - 7
                 * Железо - TotalIron - 0,2
                 * Нитраты - Nitrates - 50
                 * Запах - Smell - 2
                 */
                $data = [
                    'Color' => floatval(str_replace([',', '<', '>'], ['.', '', ''], $arProp['Color']['VALUE']) / 20),
                    'NonTransparent' => floatval(str_replace([',', '<', '>'], ['.', '', ''], $arProp['NonTransparent']['VALUE']) / 0.58),
                    'DryRest' => floatval(str_replace([',', '<', '>'], ['.', '', ''], $arProp['DryRest']['VALUE']) / 1000),
                    'Rigidity' => floatval(str_replace([',', '<', '>'], ['.', '', ''], $arProp['Rigidity']['VALUE']) / 7),
                    'TotalIron' => floatval(str_replace([',', '<', '>'], ['.', '', ''], $arProp['TotalIron']['VALUE']) / 0.2),
                    'Nitrates' => floatval(str_replace([',', '<', '>'], ['.', '', ''], $arProp['Nitrates']['VALUE']) / 50),
                    'Smell' => floatval(str_replace([',', '<', '>'], ['.', '', ''], $arProp['Smell']['VALUE']) / 2),
                ];
                $general = 0;
                foreach ($data as $k => $value) {
                    switch (true):
                        case $value < 0.8:
                            $arAnalize[$k] = 'good';
                            $arAnalize['data'][$k] = $value;
                            break;
                        case $value >= 0.8 AND $value <= 1:
                            $arAnalize[$k] = 'normal';
                            $arAnalize['data'][$k] = $value;
                            break;
                        case $value > 1:
                            $arAnalize[$k] = 'warning';
                            $arAnalize['data'][$k] = $value;
                            break;
                    endswitch;
                    (int)$general += $value;
                }
                $arAnalize['data']['generalSpecs'] = $general / 7;
                if (
                    $arAnalize['DryRest'] == 'warning' ||
                    $arAnalize['Rigidity'] == 'warning' ||
                    $arAnalize['TotalIron'] == 'warning' ||
                    $arAnalize['Nitrates'] == 'warning'
                ) {
                    $arAnalize['generalSpecs'] = 'warning';
                } elseif (
                    $arAnalize['DryRest'] == 'normal' ||
                    $arAnalize['Rigidity'] == 'normal' ||
                    $arAnalize['TotalIron'] == 'normal' ||
                    $arAnalize['Nitrates'] == 'normal'
                ) {
                    $arAnalize['generalSpecs'] == 'normal';
                } else {
                    switch (true):
                        case $arAnalize['data']['generalSpecs'] < 0.8:
                            $arAnalize['generalSpecs'] = 'good';
                            break;
                        case $arAnalize['data']['generalSpecs'] >= 0.8 AND $arAnalize['data']['generalSpecs'] <= 1:
                            $arAnalize['generalSpecs'] = 'normal';
                            break;
                        case $arAnalize['data']['generalSpecs'] > 1:
                            $arAnalize['generalSpecs'] = 'warning';
                            break;
                    endswitch;
                }

                $arAnalize['userName'] = null;
                $arAnalize['userPhoto'] = null;
                $arAnalize['userCity'] = null;

                $arAllAnalizes[] = $arAnalize;
                $arDooble[$arAnalize['lat']][$arAnalize['lon']] = $arAnalize['id'];
            }
        }
        $response['data']['waterAnalyzes'][] = $arAllAnalizes;

        $obCache->EndDataCache(array('arAllAnalizes' => $arAllAnalizes));
    }

    echo json_encode($response);
    AddMessage2Log(array('USER_ID' => $arUser['ID'], 'REQUEST' => $params, 'RESPONSE' => $response), 'api.getMapWaterAnalyzes');
}

if ($url == '/getUserWaterAnalyzes') {
    $arUser = getUserByApiKey($apiKey);
    $filterId = $request->get('id');

    if ($arUser['ID'] > 0) {
        CModule::IncludeModule("iblock");
        $res = CIBlockElement::GetList(
            [],
            [
                "IBLOCK_ID" => 31, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y",
                '!PROPERTY_LAT' => false, "!PROPERTY_SourceTypeName" => false,
                '=PROPERTY_CustomerPhone' => $arUser['LOGIN']
            ],
            false,
            false,
            []
        );

        $response = array(
            'success' => 1,
            'errCode' => 0,
            'errMsg' => "",
        );
        $response['data']['waterAnalyzes'] = null;
        $sourceType = [
            'Водопровод' => 0,
            'Колодец' => 1,
            'Скважина' => 2,
            'Другой источник' => 3,
        ];
        $arDooble = [];
        $arAllAnalizes = null;
        while ($ob = $res->GetNextElement()) {
            $arFields = $ob->GetFields();
            $arProp = $ob->GetProperties();

            $arAnalize['id'] = $arFields['ID'];

            $arAnalize['lat'] = $arProp['LAT']['VALUE'];
            $arAnalize['lon'] = $arProp['LNG']['VALUE'];

            if (!$arDooble[$arAnalize['lat']][$arAnalize['lon']]) {
                $arAnalize['address'] = $arProp['CityName']['VALUE'] . ', ' . $arProp['Address']['VALUE'];

                $arAnalize['SourceTypeName'] = $arProp['SourceTypeName']['VALUE'];
                $arAnalize['SourceTypeID'] = $sourceType[$arProp['SourceTypeName']['VALUE']];


                $good = 0;
                $normal = 0;
                $warning = 0;

                /*
                 * Цветность - Color - 20
                 * Мутность - NonTransparent - 0,58
                 * Сухой остаток - DryRest - 1000
                 * Жесткость - Rigidity - 7
                 * Железо - TotalIron - 0,2
                 * Нитраты - Nitrates - 50
                 * Запах - Smell - 2
                 */
                $data = [
                    'Color' => floatval(str_replace([',', '<', '>'], ['.', '', ''], $arProp['Color']['VALUE']) / 20),
                    'NonTransparent' => floatval(str_replace([',', '<', '>'], ['.', '', ''], $arProp['NonTransparent']['VALUE']) / 0.58),
                    'DryRest' => floatval(str_replace([',', '<', '>'], ['.', '', ''], $arProp['DryRest']['VALUE']) / 1000),
                    'Rigidity' => floatval(str_replace([',', '<', '>'], ['.', '', ''], $arProp['Rigidity']['VALUE']) / 7),
                    'TotalIron' => floatval(str_replace([',', '<', '>'], ['.', '', ''], $arProp['TotalIron']['VALUE']) / 0.2),
                    'Nitrates' => floatval(str_replace([',', '<', '>'], ['.', '', ''], $arProp['Nitrates']['VALUE']) / 50),
                    'Smell' => floatval(str_replace([',', '<', '>'], ['.', '', ''], $arProp['Smell']['VALUE']) / 2),
                ];
                $general = 0;
                foreach ($data as $k => $value) {
                    switch (true):
                        case $value < 0.8:
                            $arAnalize[$k] = 'good';
                            $arAnalize['data'][$k] = $value;
                            break;
                        case $value >= 0.8 AND $value <= 1:
                            $arAnalize[$k] = 'normal';
                            $arAnalize['data'][$k] = $value;
                            break;
                        case $value > 1:
                            $arAnalize[$k] = 'warning';
                            $arAnalize['data'][$k] = $value;
                            break;
                    endswitch;
                    (int)$general += $value;
                }
                $arAnalize['data']['generalSpecs'] = $general / 7;
                if (
                    $arAnalize['DryRest'] == 'warning' ||
                    $arAnalize['Rigidity'] == 'warning' ||
                    $arAnalize['TotalIron'] == 'warning' ||
                    $arAnalize['Nitrates'] == 'warning'
                ) {
                    $arAnalize['generalSpecs'] = 'warning';
                } elseif (
                    $arAnalize['DryRest'] == 'normal' ||
                    $arAnalize['Rigidity'] == 'normal' ||
                    $arAnalize['TotalIron'] == 'normal' ||
                    $arAnalize['Nitrates'] == 'normal'
                ) {
                    $arAnalize['generalSpecs'] == 'normal';
                } else {
                    switch (true):
                        case $arAnalize['data']['generalSpecs'] < 0.8:
                            $arAnalize['generalSpecs'] = 'good';
                            break;
                        case $arAnalize['data']['generalSpecs'] >= 0.8 AND $arAnalize['data']['generalSpecs'] <= 1:
                            $arAnalize['generalSpecs'] = 'normal';
                            break;
                        case $arAnalize['data']['generalSpecs'] > 1:
                            $arAnalize['generalSpecs'] = 'warning';
                            break;
                    endswitch;
                }

                $arAnalize['userName'] = trim($arUser['NAME'] . ' ' . $arUser['LAST_NAME']);
                $arAnalize['userPhoto'] = CFile::getPath($arUser['PERSONAL_PHOTO']);
                $arAnalize['userCity'] = $arUser['PERSONAL_CITY'];

                $arAllAnalizes[] = $arAnalize;
                $arDooble[$arAnalize['lat']][$arAnalize['lon']] = $arAnalize['id'];
            }
        }
        if ($arAllAnalizes)
            $response['data']['waterAnalyzes'] = $arAllAnalizes;

        /*
        $analyzes = getHighLoadBlockList(
            'api_map_water_analyzes',
            array('UF_USER' => $arUser['ID'])
        );
        foreach ($analyzes as $analize) {
//            pre($analize);
            $arAnalize['id'] = $analize['ID'];
            $arAnalize['status'] = $analize['UF_ACTIVE'];
            $arAnalize['lat'] = $analize['UF_LAT'];
            $arAnalize['lon'] = $analize['UF_LON'];
            $arAnalize['address'] = $analize['UF_ADDRESS'];
            $arAnalize['generalSpecs'] = $analize['UF_GENERAL_SPECS'];
            $arAnalize['mineralizationLevel'] = $analize['UF_MINERAL_LEVEL'];
            $arAnalize['hardnessLevel'] = $analize['UF_HARDNESS_LEVEL'];
            $arAnalize['chromaticityLevel'] = $analize['UF_CHROMATI_LEVEL'];
            $arAnalize['ferrumLevel'] = $analize['UF_FERRUM_LEVEL'];
            $arAnalize['nitratesLevel'] = $analize['UF_NITRATES_LEVEL'];

            $arAnalize['data'] = [
                'nitratesLevel' => $arProp['Nitrates']['VALUE'],
                'ferrumLevel' => $arProp['TotalIron']['VALUE'],
                'chromaticityLevel' => $arProp['Color']['VALUE'],
                'hardnessLevel' => $arProp['Rigidity']['VALUE'],
                'mineralizationLevel' => $arProp['DryRest']['VALUE'],
            ];
//            pre($arUser);
            $arAnalize['userName'] = trim($arUser['NAME'] . ' ' . $arUser['LAST_NAME']);
            $arAnalize['userPhoto'] = CFile::getPath($arUser['PERSONAL_PHOTO']);
            $arAnalize['userCity'] = $arUser['PERSONAL_CITY'];

            $response['data']['waterAnalyzes'][] = $arAnalize;
        }
        */
        // logic
    } else {
        $response = array(
            'success' => 0,
            'errCode' => 404,
            'errMsg' => "apiKey not found",
        );
    }
    echo json_encode($response);
    AddMessage2Log(array('USER_ID' => $arUser['ID'], 'REQUEST' => $params, 'RESPONSE' => $response), 'api.getUserWaterAnalyzes');
}

if ($url == '/orderCallback') {
    $arUser = getUserByApiKey($apiKey);
    $phone = $request->get('phone');

    if (!empty($phone)) {
        $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById(CALLBACK_TABLE_ID)->fetch();
        $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
        $entity_data_class = $entity->getDataClass();

        $data = array(
            "UF_USER" => ($arUser['ID']) ? $arUser['ID'] : false,
            "UF_PHONE" => $phone,
        );
        $result = $entity_data_class::add($data);

        $response = array(
            'success' => 1,
            'errCode' => 0,
            'errMsg' => "",
        );
    } else {
        $response = array(
            'success' => 0,
            'errCode' => 404,
            'errMsg' => "phone is empty",
        );
    }
    echo json_encode($response);
    AddMessage2Log(array('USER_ID' => $arUser['ID'], 'REQUEST' => $params, 'RESPONSE' => $response), 'api.orderCallback');
}

if ($url == '/orderWaterAnalisys') {
    $arUser = getUserByApiKey($apiKey);
    $fio = $request->get('fio');
    $phone = $request->get('phone');
    $city = $request->get('city');
    $address = $request->get('address');
    $build = $request->get('build');
    $dopinfo = $request->get('dopinfo');

    if ($arUser['ID'] > 0) {
        $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById(ORDER_WATER_ANALISIS_TABLE_ID)->fetch();
        $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
        $entity_data_class = $entity->getDataClass();

        $data = array(
            "UF_USER" => $arUser['ID'],
            "UF_FIO" => $fio,
            "UF_PHONE" => $phone,
            "UF_CITY" => $city,
            "UF_ADDRESS" => $address,
            "UF_BUILD" => $build,
            "UF_DOPINFO" => $dopinfo,
        );
        $result = $entity_data_class::add($data);

        \Bitrix\Main\Mail\Event::send(array(
            "EVENT_NAME" => "WATTER_COURIER",
            "LID" => "s8",
            "C_FIELDS" => array(
                "EMAIL" => COption::getOptionString('watter', 'EMAIL'),
                "USER_ID" => $arUser['ID'],
                "FIO" => $fio,
                "PHONE" => $phone,
                "CITY" => $city,
                "ADDRESS" => $address,
                "BUILD" => $build,
                "DOPINFO" => $dopinfo,
            ),
        ));

        $response = array(
            'success' => 1,
            'errCode' => 0,
            'errMsg' => "",
        );
    } else {
        $response = array(
            'success' => 0,
            'errCode' => 404,
            'errMsg' => "apiKey not found",
        );
    }
    echo json_encode($response);
    AddMessage2Log(array('USER_ID' => $arUser['ID'], 'REQUEST' => $params, 'RESPONSE' => $response), 'api.orderWaterAnalisys');
}

if ($url == '/getPartners') {

    $response = array(
        'success' => 1,
        'errCode' => 0,
        'errMsg' => "",
    );
    $partners = getHighLoadBlockList(
        'api_watter_partners'
    );
    foreach ($partners as $partner) {
        $arPartner['id'] = $partner['ID'];
        $arPartner['name'] = $partner['UF_NAME'];
        $arPartner['logo'] = CFile::getPath($partner['UF_LOGO']);

        $response['data']['partners'][] = $arPartner;
    }

    echo json_encode($response);
    AddMessage2Log(array('USER_ID' => $arUser['ID'], 'REQUEST' => $params, 'RESPONSE' => $response), 'api.getPartners');
}

if ($url == '/getCities') {

    CModule::IncludeModule("sale");
    $arUser = getUserByApiKey($apiKey);
    $type = ($request->get('allRegion') == 1) ? 3 : 5;
    $region = ($request->get('region')) ? $request->get('region') : 1;
    $lang = ($request->get('lang')) ? $request->get('lang') : 'ru';

    //if ($arUser['ID'] > 0) {
    $response = array(
        'success' => 1,
        'errCode' => 0,
        'errMsg' => "",
    );
    $filter = array(
        '=NAME.LANGUAGE_ID' => $lang,
        'COUNTRY_ID' => 1,
        'TYPE_ID' => $type
    );
    if ($region > 1) {
        $filter['REGION_ID'] = $region;
    }
    $res = \Bitrix\Sale\Location\LocationTable::getList(array(
        'filter' => $filter,
        'select' => array(
            '*', 'NAME_RU' => 'NAME.NAME',
        ),
        'order' => array(
            'NAME.NAME' => 'asc'
        )
    ));
    while ($item = $res->fetch()) {
//        print_r($item);

        $arLocation['id'] = $item['ID'];
        $arLocation['name'] = $item['NAME_RU'];
        $arLocation['country'] = $item['COUNTRY_ID'];
        $arLocation['region'] = $item['REGION_ID'];
        $response['data']['cities'][] = $arLocation;
    }


//    } else {
//        $response = array(
//            'success' => 0,
//            'errCode' => 404,
//            'errMsg' => "apiKey not found",
//        );
//    }
    echo json_encode($response);
    AddMessage2Log(array('USER_ID' => $arUser['ID'], 'REQUEST' => $params, 'RESPONSE' => $response), 'api.getCities');
}

if ($url == '/getFilterTypes') {
    $response = array(
        'success' => 1,
        'errCode' => 0,
        'errMsg' => "",
    );
    $response['data']['filterType'] = [];
    $filterTypes = getHighLoadBlockList(
        'prop_filter_types'
    );
    foreach ($filterTypes as $filterType) {
        $usFilter['id'] = $filterType['ID'];
        $usFilter['uid'] = $filterType['UF_XML_ID'];
        $usFilter['name'] = $filterType['UF_NAME'];
        $usFilter['description'] = $filterType['UF_DESCRIPTION'];
        $usFilter['icoUrl'] = CFile::getPath($filterType['UF_ICON']);

        $response['data']['filterType'][] = $usFilter;
    }

    echo json_encode($response);
    AddMessage2Log(array('USER_ID' => $arUser['ID'], 'REQUEST' => $params, 'RESPONSE' => $response), 'api.getFilterTypes');
}

if ($url == '/getFilterBrands') {

    $response = array(
        'success' => 1,
        'errCode' => 0,
        'errMsg' => "",
    );
    $response['data']['filterType'] = [];
    $filterTypes = getHighLoadBlockList(
        'prop_filter_brands'
    );
    foreach ($filterTypes as $filterType) {
        $usFilter['id'] = $filterType['ID'];
        $usFilter['uid'] = $filterType['UF_XML_ID'];
        $usFilter['name'] = $filterType['UF_NAME'];
        $usFilter['description'] = $filterType['UF_DESCRIPTION'];
        $usFilter['img'] = CFile::getPath($filterType['UF_ICON']);

        $response['data']['brands'][] = $usFilter;
    }

    echo json_encode($response);
    AddMessage2Log(array('USER_ID' => $arUser['ID'], 'REQUEST' => $params, 'RESPONSE' => $response), 'api.getFilterBrands');
}

if ($url == '/getUserMessages') {
    $arUser = getUserByApiKey($apiKey);

    if ($arUser['ID'] > 0) {
        $response = array(
            'success' => 1,
            'errCode' => 0,
            'errMsg' => "",
        );
        $response['data']['messages'] = [];

        $messages = getHighLoadBlockList(
            'api_client_messages',
            array('=UF_USER' => $arUser['ID']),
            array('*'),
            ['UF_TIME' => 'DESC']
        );
        foreach ($messages as $message) {
//            print_r($message['UF_TIME']);
            $arMessage['id'] = $message['ID'];
            if ($message['UF_TIME'])
                $arMessage['date'] = $message['UF_TIME']->toString();
            else
                $arMessage['date'] = null;
            $arMessage['type'] = $message['UF_TYPE'];
            $arMessage['message'] = $message['UF_MESSAGE'];

            $response['data']['messages'][] = $arMessage;
        }
        // logic
    } else {
        $response = array(
            'success' => 0,
            'errCode' => 404,
            'errMsg' => "apiKey not found",
        );
    }
    echo json_encode($response);
    AddMessage2Log(array('USER_ID' => $arUser['ID'], 'REQUEST' => $params, 'RESPONSE' => $response), 'api.getUserMessages');
}

if ($url == '/getFilters') {
    CModule::IncludeModule("iblock");
    $sortBy = ($request->get('sortBy')) ? $request->get('sort') : 'id';
    $sort = ($request->get('sort')) ? mb_strtoupper($request->get('sort')) : 'ASC';

    $arNavStartParams['nPageSize'] = ($request->get('per_page')) ? (int)$request->get('per_page') : 20;
    $arNavStartParams['iNumPage'] = ($request->get('page')) ? (int)$request->get('page') : 1;

    $img_h = ($request->get('img_h')) ? (int)$request->get('img_h') : 250;
    $img_w = ($request->get('img_w')) ? (int)$request->get('img_w') : 250;

    $resFilter = ["IBLOCK_ID" => FILTER_IBLOCK_ID, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y"];
    if ($request->get('brand')) $resFilter['PROPERTY_BRAND'] = $request->get('brand');
    if ($request->get('type')) $resFilter['PROPERTY_TYPE'] = $request->get('type');

    $res = CIBlockElement::GetList(
        [$sortBy => $sort],
        $resFilter,
        false,
        $arNavStartParams,
        ["ID", "NAME", "DETAIL_PICTURE", "PREVIEW_TEXT", "PROPERTY_BRAND", "PROPERTY_TYPE"]
    );
    $response = array(
        'success' => 1,
        'errCode' => 0,
        'errMsg' => "",
    );

    $response['data']['found'] = $res->NavRecordCount;
    $response['data']['per_page'] = $res->NavPageSize;
    $response['data']['pages'] = $res->NavPageCount;
    $response['data']['page'] = $res->NavPageNomer;

    $response['data']['filters'] = [];

    while ($ob = $res->GetNextElement()) {
        $arFields = $ob->GetFields();
//        print_r($arFields);

        $arFilter['id'] = $arFields['ID'];
        $arFilter['name'] = $arFields['NAME'];
        $arFilter['description'] = $arFields['PREVIEW_TEXT'];

        if ($arFields['DETAIL_PICTURE']) {
            $file = CFile::ResizeImageGet($arFields['DETAIL_PICTURE'], array('width' => $img_w, 'height' => $img_h), BX_RESIZE_IMAGE_PROPORTIONAL, false);
            $arFilter['img'] = $file['src'];
        }
        $arFilter['brand'] = $arFields['PROPERTY_BRAND_VALUE'];
        $arFilter['type'] = $arFields['PROPERTY_TYPE_VALUE'];


        $response['data']['filters'][] = $arFilter;
    }
//    pre($res);
    echo json_encode($response);
    AddMessage2Log(array('USER_ID' => $arUser['ID'], 'REQUEST' => $params, 'RESPONSE' => $response), 'api.getFilters');

}

if ($url == '/deleteUserFilter') {
    $arUser = getUserByApiKey($apiKey);
    $filterId = $request->get('id');

    if ($arUser['ID'] > 0) {
        $tbl = 'api_watter_client_filters';
        $hlbl = 9;
        $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById($hlbl)->fetch();
        $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
        $entity_data_class = $entity->getDataClass();
        if (($filterId))
            $entity_data_class::Delete($filterId);
        // logic
        $response = array(
            'success' => 1,
            'errCode' => 0,
            'errMsg' => "",
        );
    } else {
        $response = array(
            'success' => 0,
            'errCode' => 404,
            'errMsg' => "apiKey not found",
        );
    }
    echo json_encode($response);
    AddMessage2Log(array('USER_ID' => $arUser['ID'], 'REQUEST' => $params, 'RESPONSE' => $response), 'api.deleteUserFilter');
}

if ($url == '/callCourier') {
    $arUser = getUserByApiKey($apiKey);
    $fio = $request->get('fio');
    $phone = $request->get('phone');
    $phone = $request->get('phone');
    $city = $request->get('city');
    $address = $request->get('address');
    $build = $request->get('build');
    $dopinfo = $request->get('dopinfo');

    if ($arUser['ID'] > 0) {
        $npApi = COption::getOptionString('watter', 'APIKEY_NP');

        $url = 'https://api.novaposhta.ua/v2.0/json/';
        $data = array(
            'apiKey' => $npApi,
            'modelName' => 'InternetDocument',
            'calledMethod' => 'save',
            'language' => 'ru',
            'methodProperties' => [
                "NewAddress" => "1",
                "PayerType" => "Recipient",
                "PaymentMethod" => "Cash",
                "CargoType" => "Cargo",
                "VolumeGeneral" => "0.1",
                "Weight" => "1",
                "ServiceType" => "DoorsWarehouse",
                "SeatsAmount" => "1",
                "Description" => "вода для анализа",
                "Cost" => "100",
                "CitySender" => "8d5a980d-391c-11dd-90d9-001a92567626",
                "Sender" => "5ace4a2e-13ee-11e5-add9-005056887b8d",
                "SenderAddress" => "d492290b-55f2-11e5-ad08-005056801333",
                "ContactSender" => "613b77c4-1411-11e5-ad08-005056801333",
                "SendersPhone" => "380001234567",
                "RecipientCityName" => "київ",
                "RecipientArea" => "",
                "RecipientAreaRegions" => "",
                "RecipientAddressName" => "95",
                "RecipientHouse" => "",
                "RecipientFlat" => "",
                "RecipientName" => "Тест Тест Тест",
                "RecipientType" => "PrivatePerson",
                "RecipientsPhone" => "380001234567",
                "DateTime" => date('d.m.Y'),
            ]
        );
        $post = json_encode($data);

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        $result = curl_exec($ch);
        curl_close($ch);

        $res = json_decode($result);

        $response = $res;
    } else {
        $response = array(
            'success' => 0,
            'errCode' => 404,
            'errMsg' => "apiKey not found",
        );
    }
    echo json_encode($response);
    AddMessage2Log(array('USER_ID' => $arUser['ID'], 'REQUEST' => $params, 'RESPONSE' => $response), 'api.callCourier');
}
// ****** functions ****** //
/**
 * @param $apiKey
 * @return array user data
 */
function getUserByApiKey($apiKey)
{
    $userData = [];
    if (!empty($apiKey)) {

        $userData = CUser::GetList($by = 'ID', $order = 'ASC',
            array(
                '=UF_API_KEY' => $apiKey,
                'GROUPS_ID' => array(9)
            ),
            array(
                "SELECT" => array("UF_*")
            )
        )->Fetch();
    }
    return $userData;
}

/**
 * @param       $hlbl table_name
 * @param array $filter
 * @param array $arSelect
 * @param array $arOrder
 * @return array
 */

/*
 if ($url == '/functionName') {
    $arUser = getUserByApiKey($apiKey);
    $filterId = $request->get('id');

    if ($arUser['ID'] > 0) {
        // logic
    } else {
        $response = array(
            'success' => 0,
            'errCode' => 404,
            'errMsg' => "apiKey not found",
        );
    }
    echo json_encode($response);
    AddMessage2Log(array('USER_ID' => $arUser['ID'], 'REQUEST' => $params, 'RESPONSE' => $response), 'api.functionName');
}
 */